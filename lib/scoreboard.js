export class Scoreboard {
  #matches = new Map();
  // Date.now(); is a bad solution here
  // it's easy to create a situation where the same timestamp is generated for different matches
  #matchIndex = 0;

  startMatch(homeTeam, awayTeam) {
    this.#validateTeams(homeTeam, awayTeam);

    const match = this.#createMatch(homeTeam, awayTeam);
    this.#matches.set(match.matchId, match);
    return match;
  }

  udpateScore(matchId, homeTeamScore, awayTeamScore) {
    if (!this.#matches.has(matchId)) {
      throw new Error('Match not found');
    }

    if (
      typeof homeTeamScore !== 'number' ||
      typeof awayTeamScore !== 'number'
    ) {
      throw new Error('Score must be a number');
    }

    if (homeTeamScore < 0 || awayTeamScore < 0) {
      throw new Error('Score cannot be negative');
    }

    const match = this.#matches.get(matchId);

    match.homeTeam.score = homeTeamScore;
    match.awayTeam.score = awayTeamScore;
  }

  finishMatch(matchId) {
    if (!this.#matches.has(matchId)) {
      throw new Error('Match not found');
    }

    this.#matches.delete(matchId);
  }

  getSummary() {
    const matches = this.#getMatchesAsArray();
    const sortedMatches = matches.toSorted((matchA, matchB) => {
      const totalScoreA = this.#getMatchTotalScore(matchA);
      const totalScoreB = this.#getMatchTotalScore(matchB);

      if (totalScoreA === totalScoreB) {
        return matchB.order - matchA.order;
      }

      return totalScoreB - totalScoreA;
    });

    return sortedMatches;
  }

  #createMatch(homeTeam, awayTeam) {
    this.#matchIndex += 1;

    const match = {
      matchId: this.#createMatchId(homeTeam, awayTeam),
      homeTeam: {
        name: homeTeam,
        score: 0,
      },
      awayTeam: {
        name: awayTeam,
        score: 0,
      },
      order: this.#matchIndex,
    };

    return match;
  }

  #createMatchId(homeTeam, awayTeam) {
    return `${homeTeam} - ${awayTeam}`;
  }

  #getAllTeams() {
    const teams = new Set();
    this.#matches.forEach((match) => {
      teams.add(match.homeTeam.name);
      teams.add(match.awayTeam.name);
    });

    return teams;
  }

  #validateTeams(homeTeam, awayTeam) {
    const teams = this.#getAllTeams();

    if (!homeTeam || !awayTeam) {
      throw new Error('Both teams are required');
    }

    if (typeof homeTeam !== 'string' || typeof awayTeam !== 'string') {
      throw new Error('Both teams must be strings');
    }

    if (homeTeam === awayTeam) {
      throw new Error('Teams cannot be the same');
    }

    if (teams.has(homeTeam)) {
      throw new Error(`Team ${homeTeam} is already busy on another match`);
    }

    if (teams.has(awayTeam)) {
      throw new Error(`Team ${awayTeam} is already busy on another match`);
    }
  }

  #getMatchTotalScore(match) {
    return match.homeTeam.score + match.awayTeam.score;
  }

  #getMatchesAsArray() {
    return Array.from(this.#matches.values());
  }
}
