import { expect, it, describe, vi, beforeEach } from 'vitest';
import { Scoreboard } from './scoreboard.js';

describe('Scoreboard', () => {
  let scoreboard;

  beforeEach(() => {
    vi.clearAllMocks();
    scoreboard = new Scoreboard();
  });

  describe('startMatch()', () => {
    it('should return a new match object', () => {
      const match = scoreboard.startMatch('Mexico', 'Canada');

      expect(match).toEqual({
        matchId: 'Mexico - Canada',
        order: 1,
        homeTeam: {
          name: 'Mexico',
          score: 0,
        },
        awayTeam: {
          name: 'Canada',
          score: 0,
        },
      });
    });

    it('should throw an error if teams are the same', () => {
      expect(() => scoreboard.startMatch('Mexico', 'Mexico')).toThrow(
        'Teams cannot be the same'
      );
    });

    it('should throw an error if one of the teams is missing', () => {
      expect(() => scoreboard.startMatch('Mexico')).toThrow(
        'Both teams are required'
      );
      expect(() => scoreboard.startMatch()).toThrow('Both teams are required');
      expect(() => scoreboard.startMatch('', 'Mexico')).toThrow(
        'Both teams are required'
      );
    });

    it('should throw an error if at least one of the teams is busy', () => {
      scoreboard.startMatch('Mexico', 'Canada');

      expect(() => scoreboard.startMatch('Mexico', 'Italy')).toThrow();
      expect(() => scoreboard.startMatch('Italy', 'Canada')).toThrow();
    });

    it('should throw an error if parameters have an invalid type', () => {
      expect(() => scoreboard.startMatch(1, 2)).toThrow();
      expect(() => scoreboard.startMatch('Mexico', {})).toThrow();
      expect(() => scoreboard.startMatch([], 'Mexico')).toThrow();
    });
  });

  describe('udpateScore()', () => {
    it('should properly update the score of a match', () => {
      const match = scoreboard.startMatch('Mexico', 'Canada');
      scoreboard.udpateScore(match.matchId, 3, 1);

      expect(match.homeTeam.score).toBe(3);
      expect(match.awayTeam.score).toBe(1);
    });

    it('should throw an error if match is not found', () => {
      expect(() => scoreboard.udpateScore('invalid', 0, 1)).toThrow(
        'Match not found'
      );
    });

    it('should throw an error if score is negative', () => {
      const match = scoreboard.startMatch('Mexico', 'Canada');

      expect(() => scoreboard.udpateScore(match.matchId, -2, -1)).toThrow(
        'Score cannot be negative'
      );
    });

    it('should throw an error if score is not a number', () => {
      const match = scoreboard.startMatch('Mexico', 'Canada');
      expect(() => scoreboard.udpateScore(match.matchId, '2', '1')).toThrow(
        'Score must be a number'
      );
    });
  });

  describe('getSummary()', () => {
    it('getSummary() should return a list of matches ordered by total score', () => {
      const match1 = scoreboard.startMatch('Mexico', 'Canada');
      const match2 = scoreboard.startMatch('Spain', 'Brazil');
      const match3 = scoreboard.startMatch('Germany', 'France');

      scoreboard.udpateScore(match1.matchId, 0, 5);
      scoreboard.udpateScore(match2.matchId, 10, 2);
      scoreboard.udpateScore(match3.matchId, 2, 2);

      const summary = scoreboard.getSummary();

      expect(summary).toEqual([
        {
          matchId: 'Spain - Brazil',
          order: 2,
          homeTeam: {
            name: 'Spain',
            score: 10,
          },
          awayTeam: {
            name: 'Brazil',
            score: 2,
          },
        },
        {
          matchId: 'Mexico - Canada',
          order: 1,
          homeTeam: {
            name: 'Mexico',
            score: 0,
          },
          awayTeam: {
            name: 'Canada',
            score: 5,
          },
        },
        {
          matchId: 'Germany - France',
          order: 3,
          homeTeam: {
            name: 'Germany',
            score: 2,
          },
          awayTeam: {
            name: 'France',
            score: 2,
          },
        },
      ]);
    });

    it('the matches with the same total score should be ordered by the most recent', () => {
      const match1 = scoreboard.startMatch('Mexico', 'Canada');
      const match2 = scoreboard.startMatch('Spain', 'Brazil');
      const match3 = scoreboard.startMatch('Germany', 'France');
      const match4 = scoreboard.startMatch('Uruguay', 'Italy');
      const match5 = scoreboard.startMatch('Argentina', 'Australia');

      scoreboard.udpateScore(match1.matchId, 0, 5);
      scoreboard.udpateScore(match2.matchId, 10, 2);
      scoreboard.udpateScore(match3.matchId, 2, 2);
      scoreboard.udpateScore(match4.matchId, 6, 6);
      scoreboard.udpateScore(match5.matchId, 3, 1);

      const summary = scoreboard.getSummary();

      expect(summary).toEqual([
        {
          matchId: 'Uruguay - Italy',
          order: 4,
          homeTeam: {
            name: 'Uruguay',
            score: 6,
          },
          awayTeam: {
            name: 'Italy',
            score: 6,
          },
        },
        {
          matchId: 'Spain - Brazil',
          order: 2,
          homeTeam: {
            name: 'Spain',
            score: 10,
          },
          awayTeam: {
            name: 'Brazil',
            score: 2,
          },
        },
        {
          matchId: 'Mexico - Canada',
          order: 1,
          homeTeam: {
            name: 'Mexico',
            score: 0,
          },
          awayTeam: {
            name: 'Canada',
            score: 5,
          },
        },
        {
          matchId: 'Argentina - Australia',
          order: 5,
          homeTeam: {
            name: 'Argentina',
            score: 3,
          },
          awayTeam: {
            name: 'Australia',
            score: 1,
          },
        },
        {
          matchId: 'Germany - France',
          order: 3,
          homeTeam: {
            name: 'Germany',
            score: 2,
          },
          awayTeam: {
            name: 'France',
            score: 2,
          },
        },
      ]);
    });
  });

  describe('finishMatch()', () => {
    it("should remove a match from the scoreboard so it doesn't appear in summary", () => {
      expect(scoreboard.getSummary().length).toBe(0);
      const match = scoreboard.startMatch('Mexico', 'Canada');
      scoreboard.udpateScore(match.matchId, 3, 1);

      expect(scoreboard.getSummary().length).toBe(1);

      scoreboard.finishMatch(match.matchId);

      expect(scoreboard.getSummary().length).toBe(0);
    });

    it('should throw an error if the match is not found', () => {
      expect(() => scoreboard.finishMatch('Italy - France')).toThrow(
        'Match not found'
      );
    });
  });
});
