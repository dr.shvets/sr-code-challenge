# SR Code Challenge

## System requirements

- node 21.6.1
- npm 10.2.4

## Project setup

- `git clone` repo
- install dependencies `npm ci`
- run tests `npm run test`

## Usage

```js
import { Scoreboard } from './lib/scoreboard.js';

// create a scoreboard instance
const scoreboard = new Scoreboard();

// start a match
const match = scoreboard.startMatch('Mexico', 'Canada');

// update the match score
scoreboard.udpateScore(match.matchId, 0, 5);

// generate summary
const summary = scoreboard.getSummary();

// finish match
scoreboard.finishMatch(match.matchId);
```

## instance API

| method      | params                                                  | description                                                        |
| ----------- | ------------------------------------------------------- | ------------------------------------------------------------------ |
| startMatch  | `homeTeam: string, awayTeam: string`                    | adds match to scoreboard and returns a match object with `matchId` |
| updateScore | `matchId: string, homeScore: number, awayScore: number` | updates a match score                                              |
| finishMatch | `matchId: string    `                                   | removes a match from the scoreboard                                |
| getSummary  | -                                                       | returns an array of matches                                        |

## Solution note

- the return value of getSummary() I've implemented the way so it returns an array of matches with all the details to provide a flexible interface for future usage
